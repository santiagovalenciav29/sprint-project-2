const Joi = require('joi');

const validacionUsuario = Joi.object({
    nombre: Joi.string()
            .min(3)
            .max(30)
            .required(),
    correo: Joi.string()
            .email({
                minDomainSegments:2,
                tlds: { allow: ['com','net','co']},
            }),
    telefono: Joi.number()
            .greater(9999999),
    password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    repeat_password: Joi.ref('password'),
    Administrador: Joi.boolean(),
    activo: Joi.boolean()
});

module.exports = validacionUsuario;