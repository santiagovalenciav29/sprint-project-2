const { config } = require('dotenv');

config();

module.exports = {
    HOST : process.env.HOST,
    PORT : process.env.PORT || 5000,
    NAMEDB : process.env.NAMEDB,
    USERDB : process.env.USERDB,
    PASSWORDDB : process.env.PASSWORDDB,
    PASSWORDA : process.env.PASSWORDA,
    PASSWORDB : process.env.PASSWORDB,
    PORT_REDIS: process.env.PORT_REDIS,
    PASSWORDMASTER: process.env.PASSWORDMASTER,
}