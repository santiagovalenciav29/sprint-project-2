const db = require('../db.js');
const bcrypt = require('bcrypt');
const validacionUsuario = require('../schemas/singUp.joi.js');
const validacionLogin = require('../schemas/login.joi.js');
const jsonwebToken = require('jsonwebtoken');
const config = require('../config.js');



PASSWORDMASTER = config.PASSWORDMASTER;


exports.getUsers = async(req, res) => {
    const usuarios = await db.Usuario.findAll();
    res.json(usuarios);
}

exports.loginUser = async(req,res) => {
    try{
        const {
            correo,
            password
        } = await validacionLogin.validateAsync(req.body);
            const {
                password: Upassword,
                activo,
                Administrador
            } = await db.Usuario.findOne({
                where: { correo }
            });
            const comparacionLogin = bcrypt.compareSync(password, Upassword);
            if(comparacionLogin && activo == true ){
                const token = jsonwebToken.sign({
                    correo,
                    Administrador
                },PASSWORDMASTER);
                res.json(`token: ${token}`);
            }else{
                res.status(401).json('unauthorized');
            }
    }catch(e){
        res.status(404).json('los datos ingresados son invalidos, verifica e intenta nuevamente');
    }
}

exports.createUser = async(req,res) => {
    try{
        const {
            nombre,
            correo,
            telefono,
            password,
            Administrador,
        } = await validacionUsuario.validateAsync(req.body);
        const correoExitente = await db.Usuario.findOne({ where: {correo} })
        if(correoExitente){
            res.status(404).json('el correo ya esta en uso');
        }else{
            const nuevoUsuario = await db.Usuario.create({
                nombre,
                correo,
                telefono,
                password: bcrypt.hashSync(password, 3),
                Administrador,
            });
            res.json('usuario creado con exito');
        }
    } catch(e){
        res.status(404).json(e);
    }
}


exports.updateUserState = async(req,res) => {
    try{
        const { id } = req.params;
        const { activo } = req.body;
        const findId = await db.Usuario.findOne({
            where: {
                id
            }
        });
        if(findId){
            const actualizarUsuario = await db.Usuario.update({ activo }, {
                where: {
                    id
                }
            });
            res.json(` el usuario cuyo id : ${id} ha sido actualizado, ahora su estado es: ${activo}`);
        }else{
            res.status(404).json('usuario no econtrado verifica el id del usuario');
        }

    }catch(e){
        res.status(500).json(e)
    }
}

exports.addAddress = async(req, res) => {
    const { lugarEnvio } = req.body;
    const llave = req.user.correo;
    const correoUsuario = await db.Usuario.findOne({
        where:{
            correo : llave
        }
    });
    console.log(correoUsuario)
    try{
        const nuevaDireccion = await db.direcciones.create({
            lugarEnvio,
            usuarioId: correoUsuario.id
        });
        res.json(nuevaDireccion);
    }catch(e){
        res.status(401).json(e)
    }
}

exports.seeAddresses = async(req, res) =>{
    try{
        const llave = req.user.correo;
        console.log(llave)
        const usuarioLogueado = await db.Usuario.findOne({
            where:{
                correo: llave
            }
        })
        console.log(usuarioLogueado)
        const direcciones = await db.direcciones.findAll({
            where: {
                usuarioId: usuarioLogueado.id
            }
        });
        res.json(direcciones);
    }catch (e){
        res.json(e)
    }
}

exports.deleteUser = async(req, res) => {
try{
    const { id } = req.params;
    const usuarioEliminar = await db.Usuario.findOne({
        where:{
            id
        }
    });
        if(usuarioEliminar){
            await db.Usuario.destroy({
                where:{
                    id
                }
            });
            res.json('usuario eliminado')
        }else{
            res.status(400).json('el id ingresado no puso ser encontrado') 
        }
}catch(e){
    res.status(500).json(e)
}}