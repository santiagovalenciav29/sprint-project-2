module.exports = ( sequelize, DataTypes ) => {
    const medioDePago = sequelize.define('mediosDePagos', {
        NombreMedioPago:{
            type: DataTypes.STRING,
            allowNull: false
        }
    });
    return medioDePago
}