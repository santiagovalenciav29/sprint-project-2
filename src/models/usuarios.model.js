module.exports = ( sequelize, DataTypes ) => {
    const Usuario = sequelize.define('usuarios',{
        nombre:{
            type: DataTypes.STRING,
            allowNull: false
        },
        password:{
            type: DataTypes.STRING,
            allowNull: false
        },
        correo:{
            type: DataTypes.STRING,
            allowNull: false
        },
        telefono:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        Administrador:{
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        activo:{
            type: DataTypes.BOOLEAN,
            defaultValue: true
        }
    });
    return Usuario
}