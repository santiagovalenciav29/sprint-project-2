

module.exports = ( sequelize, DataTypes ) => {
    const Direccion = sequelize.define('direcciones', {
        lugarEnvio:{
            type: DataTypes.STRING,
            allowNull: false
        }
    });
    return Direccion
}