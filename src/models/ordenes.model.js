module.exports = (sequelize, DataTypes ) => {
    const Orden = sequelize.define('ordenes', {
        costoTotal:{
            type: DataTypes.FLOAT,
            allowNull: false,
            defaultValue: 0
        },
        estadoOrden:{
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'pendiente'
        }
    });
    return Orden
}