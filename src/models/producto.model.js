module.exports = ( sequelize, DataTypes ) => {
    const Producto = sequelize.define('productos', {
        nombreProducto:{
            type: DataTypes.STRING,
            allowNull: false
        },
        precio:{
            type: DataTypes.FLOAT,
            allowNull: false
        }
    });
    return Producto
}