module.exports = ( sequelize , DataTypes ) => {
    const Operacion = sequelize.define('operaciones', {
        nombreProducto:{
            type: DataTypes.STRING,
            allowNull: false
        },
        valorProducto:{
            type:DataTypes.FLOAT,
            allowNull:false
        },
        cantidad:{
            type:DataTypes.FLOAT,
            allowNull: false,
            defaultValue: 1
        }
    });
    return Operacion
}