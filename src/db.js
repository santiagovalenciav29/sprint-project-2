const { Sequelize, DataTypes } = require('sequelize');

require('dotenv').config();
const config = require('./config.js');
const NAMEDB = config.NAMEDB;
const USERDB = config.USERDB;
const PASSWORDDB = config.PASSWORDDB;
const HOST = config.HOST;
const sequelize = new Sequelize(NAMEDB,USERDB,PASSWORDDB, {
    host: HOST,
    dialect: 'mysql',
    logging:false
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.Usuario = require('./models/usuarios.model')(sequelize, DataTypes);
db.Producto = require('./models/producto.model')(sequelize, DataTypes);
db.Orden = require('./models/ordenes.model')(sequelize, DataTypes);
db.medioDePago = require('./models/mediosDePago.model')(sequelize, DataTypes);

//tablas intermedias
db.direcciones = require('./models/direcciones.model')(sequelize, DataTypes);
db.operaciones = require('./models/operaciones.model')(sequelize, DataTypes);

// para usuarios
db.Usuario.hasMany(db.Orden);
db.Orden.belongsTo(db.Usuario);

db.Usuario.hasMany(db.direcciones);
db.direcciones.belongsTo(db.Usuario);

// para ordenes
db.direcciones.hasMany(db.Orden);
db.Orden.belongsTo(db.direcciones);

db.medioDePago.hasOne(db.Orden);
db.Orden.belongsTo(db.medioDePago);

db.Orden.hasMany(db.operaciones);
db.operaciones.belongsTo(db.Producto);

module.exports = db;


