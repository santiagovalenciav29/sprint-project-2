const express = require('express');
const routes = express.Router();
const MedioDePago = require('../models/mediosDePago.model.js');

routes.get('/obtenerMediosDePago', async(req,res)=>{
    const mediosDePago = await MedioDePago.find();
    res.json(mediosDePago);
})

routes.post('/agregarMedioDePago', async(req,res)=>{
    const { medioDePago } = req.body;
    const existeMedioDePago = await MedioDePago.findOne({ medioDePago });
    if(existeMedioDePago){
        res.status(400).json('medio de pago existente');
    } else {
        const nuevoMedioDePago = new MedioDePago ( { medioDePago });
        nuevoMedioDePago.save();
        res.status(201).json(nuevoMedioDePago);
    }
})

routes.put('/actualizarMedioDePago/:_id', async(req,res)=>{
    const { medioDePago } = req.body;
    const { _id } = req.params;
    const existeMedioDePago = await MedioDePago.findById(_id);
    if(existeMedioDePago){
        existeMedioDePago.medioDePago = medioDePago;
        existeMedioDePago.save();
        res.json(existeMedioDePago);
    } else {
        res.status(400).json('medio de pago no encontrado')
    }
});

routes.delete('/eliminarMedioDePago/:_id', async(req,res)=>{
    const { _id } = req.params;
    const existeMedioDePago = await MedioDePago.findById(_id);
    if(existeMedioDePago){
        await MedioDePago.deleteOne({_id:req.params});
        res.json('producto eliminado')
    } else {
        res.status(400).json(" id desconocido ")
    }
});

module.exports = routes;