const express = require('express');
const routes = express.Router();
const Producto = require('../models/producto.model.js');

routes.get('/obtenerProductos', async(req,res) => {
    const productos = await Producto.find();
    res.json(productos);
})

routes.post('/crearProducto', async(req,res) => {
    const { nombreProducto , precio } = req.body;
    const existeProducto = await Producto.findOne({ nombreProducto });
    if(existeProducto){
        res.status(400).json('el producto ya existe ');
    } else {
        const nuevoProducto = new Producto ( { nombreProducto, precio });
        nuevoProducto.save();
        res.json(nuevoProducto);
    }
})

routes.put('/actualizarProducto/:_id', async(req,res) => {
    const {nombreProducto, precio} = req.body;
    const { _id } = req.params;
    const existeProducto = await Producto.findById(_id);
    console.log(existeProducto);
    if (existeProducto){
        existeProducto.nombreProducto = nombreProducto;
        existeProducto.precio = precio;
        existeProducto.save();
        res.json(existeProducto);
    } else {
        res.status(400).json('producto no encontrado')
    }
})

routes.delete('/borrarProducto/:_id', async(req,res) => {
    const { _id } = req.params;
    const existeProducto = await Producto.findById(_id);
    console.log(existeProducto);
    if (existeProducto){
        await Producto.deleteOne({_id:req.params});
        res.status(201).json('producto eliminado')
    } else {
        res.status(400).json("producto no encontrado")
    }
})

module.exports = routes;