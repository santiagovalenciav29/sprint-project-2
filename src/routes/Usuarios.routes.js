const express = require('express');
const routes = express.Router();
const Usuario = require('../models/usuarios.model.js');
const user = require('../controllers/usuario.controllers.js');

/**
 * @swagger
 * /usuarios/crearUsuario:
 *  post:
 *      summary: crea un nuevo usuario en el sistema
 *      tags: [Cuenta]
 *      security: []
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/crearUsuario'
 *      responses:
 *          201:
 *              description: Usuario creado
 *          400:
 *              description: Entradas invalidas
 *          401:
 *              description: datos invalidos
 */

routes.post('/crearUsuario', user.createUser);

/**
 * @swagger
 * /usuarios/loginUsuario:
 *  post:
 *      summary: Loguea un usuario creado y da a conocer su token
 *      tags: [Cuenta]
 *      security: []
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/login'
 *      responses:
 *          201:
 *              description: Usuario creado
 *          400:
 *              description: Entradas invalidas
 *          401:
 *              description: usuario o contraseña invalidas
 */
routes.post('/loginUsuario', user.loginUser);


/**
 * @swagger
 * /usuarios/agregarDireccion:
 *  post:
 *      summary: agrega una direccion al usuario logueado
 *      tags: [Usuarios]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/agregarDireccion'
 *      responses:
 *          201:
 *              description: Direccion agregada
 *          400:
 *              description: usuario no econtrado
 *          401:
 *              description: datos invalidos
 */
routes.post('/agregarDireccion', user.addAddress);

/**
 * @swagger
 * /usuarios/verDirecciones/{id}:
 *  get:
 *      summary: Obtener todas las direcciones del usuario logueado
 *      tags: [Usuarios]
 *      responses:
 *          200:
 *              description: para esta ruta no es necesario boby
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/login'
 */
routes.get('/verDirecciones/:id', user.seeAddresses);

/**
 * @swagger
 * /usuarios/obtenerUsuarios:
 *  get:
 *      summary: Obtener todos los usuarios registrados en el sistema
 *      tags: [Usuarios]
 *      responses:
 *          200:
 *              description: para esta ruta no es necesario boby
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/login'
 */

routes.get('/obtenerUsuarios', user.getUsers);

/**
 * @swagger
 * /usuarios/actualizarEstadoUsuario/{id}:
 *  put:
 *      parameters:
 *      - in: path
 *        name: id
 *        description: id del usuario a editar
 *        required: true
 *        type: string
 * 
 *      summary: modifica el estado de un usuario creado
 *      tags: [Usuarios]
 *      requestBody:
 *          description: el estado que se poner en el usuario encontrado
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/estadoUsuario'
 *      responses:
 *          200:
 *              description: estado del usuario "nombre" ha sido modificado, ahora su estado es "estado"
 *          400:
 *              description: id no encontrado
 */

routes.put('/actualizarEstadoUsuario/:id', user.updateUserState);


/**
 * @swagger
 * /usuarios/borrarUsuario/{id}:
 *  delete:
 *      parameters:
 *      - in: path
 *        name: id
 *        description: id del usuario a eliminar
 *        required: true
 *        type: string
 *      summary: elimina el usuario referenciado por id
 *      tags: [Usuarios]
 *      responses:
 *          200:
 *              description: para esta ruta no es necesario boby
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/login'
 */
routes.delete('/borrarUsuario/:id', user.deleteUser);

// para crear usuario
/**
 * @swagger
 * tags:
 *  name: 'Cuenta'
 *  description: 'registro de cuenta'
 * 
 * components:
 *   schemas:
 *     crearUsuario:
 *         type: object
 *         required:
 *             -nombre
 *             -password
 *             -repeat_password
 *             -correo
 *             -telefono
 *         properties:
 *             nombre:
 *                 type: string
 *             password:
 *                 type: string
 *             repeat_password:
 *                 type: string
 *             correo:
 *                 type: string
 *             telefono:
 *                 type: number
 *     productResponse:
 *         type: object
 *         required:
 *             -nombre
 *             -password
 *             -repeat_password
 *             -correo
 *             -telefono
 *         properties:
 *             nombre:
 *                 type: string
 *             password:
 *                 type: string
 *             repeat_password:
 *                 type: string
 *             correo:
 *                 type: string
 *             telefono:
 *                 type: number
 *                 $ref: '#/components/schemas/crearUsuario'
 */





// ruta para loguearse
/**
 * @swagger
 * tags:
 *  name : 'Cuenta'
 *  description: 'inicio de sesion'
 * 
 * components:
 *  schemas:
 *      login:
 *          type: object
 *          required:
 *              -correo
 *              -password
 *          properties:
 *              correo:
 *                 type: string
 *              password:
 *                 type: string
 *      productResponse:
 *          type: object
 *          required:
 *              -correo
 *              -password
 *          properties:
 *              correo:
 *                  type: string
 *              password:
 *                  type: string
 *                  $ref: '#/components/schemas/login'
*/

// para modificar estado del usuario

/**
 * @swagger
 * tags:
 *  name : 'Usuarios'
 *  description: 'en relacion con los usuarios logueados'
 * 
 * components:
 *  schemas:
 *      estadoUsuario:
 *          type: object
 *          required:
 *              -activo
 *          properties:
 *              activo:
 *                 type: boolean
 *      productResponse:
 *          type: object
 *          required:
 *              -activo
 *          properties:
 *              activo:
 *                  type: boolean
 *                  $ref: '#/components/schemas/estadoUsuario'
*/

// para agregar direcciones a un usuario

/**
 * @swagger
 * tags:
 *  name : 'Usuarios'
 *  description: 'agregar una direccion a un usuario existente'
 * 
 * components:
 *  schemas:
 *      agregarDireccion:
 *          type: object
 *          required:
 *              -lugarEnvio
 *          properties:
 *              lugarEnvio:
 *                 type: string

 *      productResponse:
 *          type: object
 *          required:
 *              -lugarEnvio
 *          properties:
 *              lugarEnvio:
 *                  type: string
 *                  $ref: '#/components/schemas/agregarDireccion'
*/

module.exports = routes;