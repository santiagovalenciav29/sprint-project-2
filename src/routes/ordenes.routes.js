const express = require('express');
const routes = express.Router();
const Orden = require('../models/ordenes.model.js');
const Producto = require('../models/producto.model.js');
const Usuario = require('../models/usuarios.model.js');

routes.get('/obtenerOrdenes', async(req, res) => {
    const ordenes = await Orden.find();
    res.json(ordenes);
});

routes.post('/crearOrden',async(req,res) => {
    const { username, direccion } = req.body;
    const existeOrden = await Orden.findOne({ username: req.body.username, state: "pendiente"});
    console.log(existeOrden)
    if (existeOrden){
        res.status(400).json('solo puedes tener una orden en estado pendiente a la vez');
    } else {
        const nuevaOrden = new Orden ({ username, direccion });
        nuevaOrden.save();
        res.json(nuevaOrden);
    }
});

routes.post('/agregarProducto/:_id',async(req,res) => {
    const { _id } = req.params;
    const { username } = req.body;
    const existeProducto = await Producto.findById(_id);
    const existeOrden = await Orden.findOne({ username });
    if (existeProducto && existeOrden){
        existeOrden.products.push(existeProducto);
        existeOrden.save();
        res.json( `producto agregado a tu orden ${username}`)
    }else {
        res.status(400).json('id no encontrado')
    }
})

routes.delete('/eliminarProducto/:_id', async (req,res) => {
    try{

        const existeOrden = await Orden.findOne({ username: req.body.username, state:'pendiente' });
        console.log(existeOrden);
        if (existeOrden){
            const products = existeOrden.products;
            const productoEliminar = products.findIndex(producto => producto._id == req.params._id )
            if(productoEliminar > -1){
                products.splice(productoEliminar,1);
                existeOrden.products = products;
                await existeOrden.save();
                res.json('Producto eliminado')
            }else{
                res.status(404).json('id no encontrado')
            }
        } else {
            res.status(404).json(' orden no encontrada')
        }
    } catch(e){
        res.json.status(500).json('fallo del servidor'+ e );
    }
});

routes.put('/confirmarOrden/:_id', async(req,res) => {
    const { _id } = req.params;
    const { state } = req.body;
    const confirmarOrden = await Orden.findById(_id);
    if (confirmarOrden && state == "confirmado"){
        confirmarOrden.state = state;
        confirmarOrden.save();
        res.json(confirmarOrden);
    } else {
        res.status(400).json('id desconocido')
    }

})

module.exports = routes;