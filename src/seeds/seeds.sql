USE restaurant;

ALTER TABLE productos CHANGE COLUMN createdAt createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE productos CHANGE COLUMN updatedAt updatedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;

INSERT INTO productos (nombreProducto, createdAt, UpdatedAt, precio)
VALUES ('hamburguesa Tradicional',now(),now(), 10000 );

INSERT INTO productos (nombreProducto, createdAt, UpdatedAt, precio)
VALUES ('crepe de pollo',now(),now(), 12000 );

INSERT INTO productos (nombreProducto, createdAt, UpdatedAt, precio)
VALUES ('limonada natural',now(),now(), 3000 );

ALTER TABLE direcciones CHANGE COLUMN createdAt createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE direcciones CHANGE COLUMN updatedAt updatedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;

INSERT INTO direcciones (lugarEnvio, createdAt, UpdatedAt, usuarioId)
VALUES ('avenida candeleo, diagonal bailoteo casa 32',now(),now(), 1);

INSERT INTO direcciones (lugarEnvio, createdAt, UpdatedAt, usuarioId)
VALUES ('avenida celestial, diagonal pura playa casa 2',now(),now(), 1);


INSERT INTO direcciones (lugarEnvio, createdAt, UpdatedAt, usuarioId)
VALUES ('avenida tempestad, diagonal granizo casa 3',now(),now(), 2);

INSERT INTO direcciones (lugarEnvio, createdAt, UpdatedAt, usuarioId)
VALUES ('avenida estaciones, diagonal verano casa 7',now(),now(), 2);

ALTER TABLE mediosDepagos CHANGE COLUMN createdAt createdAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE mediosDepagos CHANGE COLUMN updatedAt updatedAt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;

INSERT INTO mediosDepagos (nombreMedioPago, createdAt, UpdatedAt)
VALUES ('efectivo',now(),now());

INSERT INTO mediosDepagos (nombreMedioPago, createdAt, UpdatedAt)
VALUES ('tarjeta',now(),now());