const expressjwt = require('express-jwt');
const config = require('../config.js');
const Usuario = require('../models/usuarios.model.js');
PASSWORDMASTER = config.PASSWORDMASTER;

exports.exJwt = expressjwt({
    secret: PASSWORDMASTER,
    algorithms: ['HS256']
}).unless({path:['/usuarios/crearUsuario','/usuarios/loginUsuario']});

exports.validationToken = ((err, req, res, next) => {
    if (err.name === 'UnauthorizedError'){
        res.status(401).json('token invalido');
    } else {
        res.status(500).json('Internal server error')
    }
});