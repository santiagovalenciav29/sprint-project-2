const express = require('express');
const helmet = require('helmet');
const db = require('./db.js');
const app = express();
require('dotenv').config();
const config = require('./config');
const swaggerOptions = require('./utils/swaggerO.js');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const validationToken = require('./middlewares/validationToken');


PORT = config.PORT;

app.use(express.json());
app.use(helmet());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));
app.use(validationToken.exJwt, validationToken.validationToken);
app.use('/usuarios', require('./routes/Usuarios.routes.js'));
app.use('/productos', require('./routes/producto.routes.js'));
app.use('/mediosDePago', require('./routes/mediosDePago.routes.js'));
app.use('/ordenes', require('./routes/ordenes.routes.js'));


db.sequelize.sync({force: false} )
    .then(() => {
        console.log('conectado a la BD');
        app.listen(PORT, () => {console.log(`arrancando express desde el puerto ${PORT}, vamos!!!`)});

        async function Usuarios(users) {
        users = await require('./seeds/users');
        }
        Usuarios();

    })
    .catch(err => {
    console.log('Error al conectar a la bd:' + err);
    });


module.exports = app;